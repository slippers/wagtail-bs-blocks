from django.utils.translation import gettext_lazy as _
from wagtail.blocks import (
        CharBlock,
        ChoiceBlock,
)
from wagtail_bs_blocks import settings
from .base_blocks import BaseLayout


class ElementChoiceBlock(ChoiceBlock):
    choices = (
        ('div', 'div'),
        ('span', 'span'),
    )


class ElementBlock(BaseLayout):

    class Meta:
        template = 'wagtail_bs_blocks/element_block.html'
        icon = "bold"

    element_type = ElementChoiceBlock(
        label=_('Element type'),
        default='div',
    )

    element_class = CharBlock(
        required=False,
        label=('Element class'),
        max_length=settings.CLASS_MAX_LENGTH,
    )
