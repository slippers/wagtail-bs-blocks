from django.utils.translation import gettext_lazy as _
from wagtail.blocks import CharBlock, StructBlock, ChoiceBlock
from wagtail_bs_blocks import settings


class HeaderChoiceBlock(ChoiceBlock):
    choices = (
        ('h1', 'H1'),
        ('h2', 'H2'),
        ('h3', 'H3'),
        ('h4', 'H4'),
        ('h5', 'H5'),
        ('h6', 'H6'),
    )


class HeaderBlock(StructBlock):

    class Meta:
        template = 'wagtail_bs_blocks/header_block.html'
        icon = "title"

    header = HeaderChoiceBlock(
        label=_('Header Size'),
        default='h1',
    )

    header_text = CharBlock(
        label=_('Header Text'),
        max_length=250,
    )

    header_class = CharBlock(
        required=False,
        label=('Header class'),
        max_length=settings.CLASS_MAX_LENGTH,
        default='text-center',
    )

