from django.utils.translation import gettext_lazy as _
from wagtail.blocks import CharBlock, StructBlock
from .collection_chooser_block import CollectionChooserBlock


class GalleryBlock(StructBlock):
    """
    Show a collection of images with interactive previews that expand to
    full size images in a modal.
    """
    class Meta:
        template = 'wagtail_bs_blocks/gallery_block.html'
        icon = 'image'
        label = _('Image Gallery')

    collection = CollectionChooserBlock(
        required=True,
        label=_('Image Collection'),
    )
