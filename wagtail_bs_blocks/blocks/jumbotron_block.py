from django.utils.translation import gettext_lazy as _
from wagtail.blocks import CharBlock
from .base_blocks import BaseLayout
from .background_style import BackgroundStyle 
from wagtail_bs_blocks import settings


class JumbotronBlock(BaseLayout):

    class Meta:
        icon = 'image'
        label = _('Jumbotron')
        template = 'wagtail_bs_blocks/jumbotron_block.html'


    jumbotron_class = CharBlock(
        required=False,
        label=('Jumbotron class'),
        max_length=settings.CLASS_MAX_LENGTH,
        default='h-100 p-5 text-white bg-dark rounded-3',
    )

    background_style = BackgroundStyle()
