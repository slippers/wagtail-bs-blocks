from django.utils.translation import gettext_lazy as _
from wagtail import blocks
from wagtail.blocks import RichTextBlock
from wagtail_bs_blocks import settings


class SimpleRichTextBlock(RichTextBlock):
    """
    Custom block inheriting from Wagtail's original one but replacing the RichText by a SimpleRichText
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, features=settings.SIMPLERICHTEXT_FEATURES, **kwargs)

    class Meta:
        icon = 'bold'
