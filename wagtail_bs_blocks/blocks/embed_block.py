from django.utils.translation import gettext_lazy as _
from wagtail.blocks import (
    StructBlock
)
from wagtail.embeds.blocks import EmbedBlock


class EmbedBlock(StructBlock):

    class Meta:
        help_text = _('Insert an embed URL e.g https://www.yo...')
        icon = 'media'
        template = 'wagtail_bs_blocks/embed_block.html'

    # templates/wagtailembeds/embed_frontend.html
    embed = EmbedBlock()
