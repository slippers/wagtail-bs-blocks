from django.utils.translation import gettext_lazy as _
from wagtail.blocks import TextBlock, StreamBlock, CharBlock
from wagtailmarkdown.blocks import MarkdownBlock
from .header_block import HeaderBlock
from .title_header_block import TitleHeaderBlock
from .simple_richtext_block import SimpleRichTextBlock
from .button_link_block import ButtonLinkBlock
from .icon_block import IconBlock
from .image_block import ImageBlock
from .embed_block import EmbedBlock
from .gallery_block import GalleryBlock
from .figure_block import FigureBlock
from .paragraph_block import ParagraphBlock
from .jumbotron_block import JumbotronBlock
from .element_block import ElementBlock
from .horizontal_ruler import HorizontalRuler
from .markdown_document_block import MarkdownDocumentBlock


class HtmlBlocks(StreamBlock):
    """
    html_blocks =  [('html_blocks', HtmlBlocks()),]
    """
    class Meta:
        icon = 'code'
        help_text = _('Html blocks')
        template = 'wagtail_bs_blocks/basic_blocks.html'

    icon = IconBlock()
    header = HeaderBlock()
    image = ImageBlock()
    embed = EmbedBlock()
    gallery = GalleryBlock()
    paragraph = ParagraphBlock()
    simple_rich_text = SimpleRichTextBlock()
    horizontal_ruler = HorizontalRuler()
    markdown  = MarkdownBlock()
    markdown_document = MarkdownDocumentBlock()


class HtmlNestedBlocks(StreamBlock):
    """
    html_nested_blocks = [('html_nested_blocks', HtmlNestedBlocks()),]
    """
    class Meta:
        icon = 'code'
        help_text = _('Html nested blocks')
        template = 'wagtail_bs_blocks/basic_blocks.html'

    figure = FigureBlock([('html_blocks', HtmlBlocks()),], required=False)
    button_link = ButtonLinkBlock([('text', TextBlock()),('html_blocks', HtmlBlocks()),], required=False)
    jumbotron = JumbotronBlock([
        ('html_blocks', HtmlBlocks()),
        ('button_link', ButtonLinkBlock([('text',TextBlock()),('html_blocks', HtmlBlocks()),],
            required=False)),], required=False)
    element = ElementBlock([
        ('html_blocks', HtmlBlocks()),
        ('button_link', ButtonLinkBlock([('text', TextBlock()),('html_blocks', HtmlBlocks()),], required=False)),
        ], required=False)
