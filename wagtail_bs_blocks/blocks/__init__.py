from django.utils.translation import gettext_lazy as _
from wagtail_bs_blocks import settings
from .cards.card_blocks import CardBlocks
from .basic_blocks import HtmlBlocks, HtmlNestedBlocks
from .layout.grid_layout import GridContainer
from .layout.section_layout import SectionContainer, SectionLayout
from .layout.card_layout import CardContainer
from .title_header_block import TitleHeaderBlock


CARD_BLOCKS = [('card_blocks', CardBlocks()),]
html_blocks =  [('html_blocks', HtmlBlocks()),]
html_nested_blocks = BASIC_BLOCKS = [('html_nested_blocks', HtmlNestedBlocks()),]


def get_blocks(extra_blocks=[]):
    return extra_blocks + settings.EXTRA_BLOCKS + html_blocks + html_nested_blocks


def get_layout_blocks(extra_blocks=[]):
    layout_blocks = [
        ('grid', GridContainer(get_blocks(extra_blocks))),
        ('section', SectionContainer(get_blocks(extra_blocks))),
        ('card', CardContainer(CARD_BLOCKS)),
    ]
    return layout_blocks
