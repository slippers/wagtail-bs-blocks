from django.utils.translation import gettext_lazy as _
from wagtail.blocks import (
    StructBlock,
    CharBlock,
)
from ..background_style import BackgroundStyle 
from ..simple_richtext_block import SimpleRichTextBlock
from ... import settings

class BaseCardBlock(StructBlock):

    class Meta:
        verbose_name = _('Base card block')

    header = CharBlock(
        required=False,
        max_length=255,
        label=_('Header'),
    )

    title = CharBlock(
        required=False,
        max_length=255,
        label=_('Title'),
    )
    subtitle = CharBlock(
        required=False,
        max_length=255,
        label=_('Subtitle'),
    )
    description = SimpleRichTextBlock(
        required=False,
        label=_('Description'),
    )
    card_class = CharBlock(
        required=False,
        label=('Card class'),
        max_length=settings.CLASS_MAX_LENGTH,
    )

    background_style = BackgroundStyle()
