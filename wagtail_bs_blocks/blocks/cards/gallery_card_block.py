from django.utils.translation import gettext_lazy as _
from wagtail.blocks import CharBlock
from ..collection_chooser_block import CollectionChooserBlock
from .base_card_block import BaseCardBlock
from ..gallery_block import GalleryBlock

class GalleryCardBlock(BaseCardBlock):
    """
    Show a collection of images with interactive previews that expand to
    full size images in a modal.
    """
    class Meta:
        template = 'wagtail_bs_blocks/cards/gallery_card_block.html'
        icon = 'image'
        label = _('Image Gallery')

    gallery = GalleryBlock() 
