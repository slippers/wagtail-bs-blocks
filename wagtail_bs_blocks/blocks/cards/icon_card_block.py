from django.utils.translation import gettext_lazy as _
from ..icon_block import IconBlock
from .base_card_block import BaseCardBlock


class IconCardBlock(BaseCardBlock):

    class Meta:
        icon = 'pick'
        label = _('Icon card')
        template = 'wagtail_bs_blocks/cards/icon_card_block.html'

    icon = IconBlock()
