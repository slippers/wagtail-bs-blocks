from django.utils.translation import gettext_lazy as _
from .base_card_block import BaseCardBlock
from ..embed_block import EmbedBlock


class EmbedCardBlock(BaseCardBlock):

    class Meta:
        icon = 'media'
        template = 'wagtail_bs_blocks/cards/embed_card_block.html',

    embed = EmbedBlock()
