from django.utils.translation import gettext_lazy as _
from .base_card_block import BaseCardBlock


class TextCardBlock(BaseCardBlock):

    class Meta:
        icon = 'bold'
        label = _('Text card')
        template = 'wagtail_bs_blocks/cards/text_card_block.html'
