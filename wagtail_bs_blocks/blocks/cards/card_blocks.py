from wagtail import blocks 
from .embed_card_block import EmbedCardBlock
from .icon_card_block import IconCardBlock
from .image_card_block import ImageCardBlock
from .gallery_card_block import GalleryCardBlock
from .text_card_block import TextCardBlock


"""
card_blocks = [('card_blocks', CardBlocks()),]
"""
class CardBlocks(blocks.StreamBlock):

    class Meta:
        icon = 'form'
        template = 'wagtail_bs_blocks/cards/card_blocks.html'

    embed_card = EmbedCardBlock()
    icon_card = IconCardBlock()
    image_card = ImageCardBlock()
    gallery_card = GalleryCardBlock()
    text_card = TextCardBlock()

