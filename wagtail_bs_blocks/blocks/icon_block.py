from django.utils.translation import gettext_lazy as _
from wagtail.blocks import (
    StructBlock,
    ChoiceBlock,
    CharBlock,
    BooleanBlock
)


class IconSizeChoiceBlock(ChoiceBlock):
    choices = (
        ('fa-lg', 'fa-lg'),
        ('fa-2x', 'fa-2x'),
        ('fa-3x', 'fa-3x'),
        ('fa-4x', 'fa-4x'),
        ('fa-5x', 'fa-5x'),
        ('fa-fw', 'fa-fw fixed width'),
    )


class IconBlock(StructBlock):
    """
    creates an html span element for fontawesome font
    the icons need to be made available on the front end.
    to see the list of fonts look at the fontawesome project
        https://github.com/FortAwesome/Font-Awesome/blob/master/metadata/icons.yml
    """

    class Meta:
        label = _('Fontawesome Icon')
        template = 'wagtail_bs_blocks/icon_block.html'
        icon = 'pick'

    icon = CharBlock(
        required=False,
        label=('Icon'),
        max_length=80,
        help_text=_('Name of the icon. ie: fa-server  fa-comment'),
    )

    size = IconSizeChoiceBlock(
        required=False,
        label=_('Size'),
    )

    border = BooleanBlock(
        required=False,
        label=_('Border = fa-border'),
    )

    spin = BooleanBlock(
        required=False,
        label=_('Spin = fa-spin'),
    )

    color = CharBlock(
        required=False,
        label=_('Color'),
        max_length=25
    )

    font_size = CharBlock(
        required=False,
        label=_('Font Size'),
        max_length=10
    )
