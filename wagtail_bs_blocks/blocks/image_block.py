from django.utils.translation import gettext_lazy as _
from wagtail.blocks import (
    StructBlock,
    CharBlock,
)
from wagtail.images.blocks import ImageChooserBlock


class ImageBlock(StructBlock):

    class Meta:
        label = _('Image Block')
        template = 'wagtail_bs_blocks/image_block.html'
        icon = 'image'

    image_src = ImageChooserBlock(
        required=False,
        max_length=255,
        label=_('Image'),
    )
    image_class = CharBlock(
        required=False,
        max_length=255,
        default='img-fluid',
        label=_('Image Class'),
    )
    image_alt = CharBlock(
        required=False,
        max_length=255,
        label=_('Image Alt Text'),
    )
