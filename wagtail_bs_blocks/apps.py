from django.apps import AppConfig


class BsBlocksConfig(AppConfig):
    name = 'wagtail_bs_blocks'
    verbose_name = 'Wagtail Boostrap Blocks'
